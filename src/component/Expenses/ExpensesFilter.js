import React from "react";
import "./ExpensesFilter.css";

const ExpensesFilter = (props) => {
  // const [filterData, setFilterData] = useState("2020");
  const getDataFromSelect = (event) => {
    // console.log("Im in e_filter");
    // setFilterData(event.target.value);
    props.onChangeFilter(event.target.value);
  };

  return (
    <div className="expenses-filter">
      <div className="expenses-filter__control">
        <label>Filter by year</label>
        <select value={props.selected} onChange={getDataFromSelect}>
          <option value="2022">2022</option>
          <option value="2021">2021</option>
          <option value="2020">2020</option>
          <option value="2019">2019</option>
        </select>
      </div>
    </div>
  );
};

export default ExpensesFilter;

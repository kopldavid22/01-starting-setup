import React, { useState } from "react";
import ExpenseForm from "./ExpenseForm";

import "./NewExpense.css";

const NewExpense = (props) => {
  const [isOpened, setIsOpened] = useState(false);

  const openExpense = () => {
    setIsOpened(true);
  };
  const closeExpense = () => {
    setIsOpened(false);
  };
  const saveExpeneseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
    setIsOpened(false);
  };

  return (
    <div className="new-expense">
      {!isOpened && (
        <button type="submit" onClick={openExpense}>
          Add new expense.
        </button>
      )}
      {isOpened && (
        <ExpenseForm
          onCancel={closeExpense}
          onSaveExpenseData={saveExpeneseDataHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;

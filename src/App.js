import React, { useState } from "react";
import Expenses from "./component/Expenses/Expenses";
import NewExpense from "./component/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  {
    id: "e1",
    title: "Car Insurance",
    amount: 94.15,
    date: new Date(2020, 7, 14),
  },
  {
    id: "e2",
    title: "Motorsssss",
    amount: 284.15,
    date: new Date(2020, 9, 14),
  },
  { id: "e3", title: "xxx", amount: 115.15, date: new Date(2021, 1, 15) },
];

const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />
    </div>
  );
};

export default App;
